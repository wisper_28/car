package com.oop;

public abstract class Car {
    protected String name;
    protected int speed;
    protected Engine engine;

    public Car(String name, int speed, Engine engine) {
        this.name = name;
        this.speed = speed;
        this.engine = engine;
    }
//    public abstract void speedUp();
    public void speedUp() {
        this.speed += engine.accelerate;
        System.out.printf("%s: speed up to %d km/h%n", this.name, this.speed);
    }
}
