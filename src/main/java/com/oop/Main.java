package com.oop;

public class Main {
    public static void main(String[] args) {
        Car coolCar = new Coolcar("coolCar", 20, new Electric());
        coolCar.speedUp();
        Driver driver = new Driver(coolCar);
        driver.speedUp();
    }
}
