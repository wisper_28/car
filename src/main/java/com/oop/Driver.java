package com.oop;

public class Driver {
    private Car car;

    public Driver(Car car) {
        this.car = car;
    }

    public void speedUp() {
        car.speedUp();
    }
}
