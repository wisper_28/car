package com.oo;

public class Car {
    private String name;
    private int speed;

    public Car(String name, int speed) {
        this.name = name;
        this.speed = speed;
    }
    public void speedUp() {
        this.speed += 5;
        System.out.printf("%s: speed up to %d km/h%n", this.name, this.speed);
    }
}
