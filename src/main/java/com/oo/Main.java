package com.oo;

public class Main {
    public static void main(String[] args) {
        Car car = new Car("Cool Car", 25);
        car.speedUp();

        Truck truck = new Truck("Big Truck", 20);
        truck.speedUp();
    }
}
