package com.oo;

public class Driver {
    private Car car;
    private Truck truck;

    public void drive() {

    }
    public void speedUp(Car car) {
        car.speedUp();
    }
    public void speedUp(Truck truck) {
        truck.speedUp();
    }
}
