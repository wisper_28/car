package com.oo;

public class Truck {
    private String name;
    private int speed;

    public Truck(String name, int speed) {
        this.name = name;
        this.speed = speed;
    }
    public void speedUp(){
        this.speed += 2;
        System.out.printf("%s: speed up to %dkm/h%n",this.name, this.speed);
    }
}
